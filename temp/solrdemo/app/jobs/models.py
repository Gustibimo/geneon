from django.db import models

class GOntology(models.Model):
    go_id = models.CharField(max_length=100)
    go_name = models.CharField(max_length=200)
    go_definition = models.CharField(max_length=2000)

    def __unicode__(self):
        return self.title
