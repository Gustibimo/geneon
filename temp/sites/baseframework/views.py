# baseframework/views.py
import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf

from baseframework.models import UserType
from baseframework.models import GOntology

# Create your views here.

def index(request):
    user_type = UserType()
    user_type.Id = 1
    user_type.Name = "Admin"

    function_info = None

    csrf_token = csrf.get_token(request)
    content = render_to_string("search/index.html", {'model' : function_info, 'csrf_token':csrf_token})
    base_url = 'http://localhost:8000/baseframework/'
    return render(request, "shared/master.html", {'content':content,'base_url':base_url})

def searchgo_solr(request):
    function_info = None
    csrf_token = csrf.get_token(request)
    action_post_url = "resultgo_solr"
    content = render_to_string("search/index.html", {'model' : function_info, 'csrf_token':csrf_token, 'action_post_url' : action_post_url})
    base_url = 'http://localhost:8000/baseframework/'
    return render(request, "shared/master.html", {'content':content,'base_url':base_url})

def resultgo_solr(request):
    query = request.POST.get("query", "")
    query_result = None
    if query != None and query != "":
        url = "http://localhost:8983/solr/geneontology/select?q=" + query + "&start=0&rows=100000&wt=json"
        query_result = requests.get(url)

    result = None
    if query_result != None:
        result = bind_json_to_list_of_object(query_result)

    csrf_token = csrf.get_token(request)
    base_url = 'http://localhost:8000/baseframework/'
    content = render_to_string("search/result.html",{'csrf_token':csrf_token, 'model' : result, 'query':query,'base_url':base_url})
    return render(request, "shared/master.html", {'content':content,'base_url':base_url})

def bind_json_to_list_of_object(response):
    go_onto = []
    result = response.json()
    if result == None:
        return go_onto

    result_response = None
    if "response" in result:
        result_response = result["response"]

    if result_response == None:
        return go_onto

    if len(result_response["docs"]) > 0:
        num = 1
        for doc in result_response["docs"]:
            gene_ontology = GOntology()

            go_id = doc["id"]
            gene_ontology.go_num = num
            gene_ontology.go_id = go_id
            gene_ontology.go_name = doc["go_name"]
            gene_ontology.go_definition = doc["go_definition"]

            if "is_a" in doc:
                gene_ontology.go_is_a = doc["is_a"]

            if "go_synonym" in doc:
                gene_ontology.go_synonym = doc["go_synonym"]

            num += 1
            go_onto.append(gene_ontology)

    return go_onto