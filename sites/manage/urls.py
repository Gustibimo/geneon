from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    #url(r'searchgo/$', views.searchgo, name='searchgo'),
    url(r'searchgo_solr/$', views.searchgo_solr, name='searchgo_solr'),
    #url(r'resultgo/$', views.resultgo, name='resultgo'),
    url(r'resultgo_solr/$', views.resultgo_solr, name='resultgo_solr'),
    url(r'detailgo/$', views.detailgo, name='detailgo'),
    #url(r'searchstructure/$', views.searchstructure, name='searchstructure'),
    #url(r'result/$', views.result, name='result'),
    #url(r'master/$', views.master, name='master'),
    #url(r'manage/userinfo$', views.manage_user_info, name='userinfo'),
]