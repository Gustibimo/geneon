# manage/models.py
from django.db import models

# Create your models here.
'''
class UserType(models.Model):
    Name = models.CharField(max_length=50)
'''

# class function info
class FunctionInfo(models.Model):
    function_id = models.CharField(max_length=30)
    name = models.CharField(max_length=50)
    url = models.CharField(max_length=50)
    sequence = models.CharField(max_length=5)
    is_enabled = models.BooleanField()
    is_active = models.BooleanField()
    parent = models.ForeignKey('self')

class GOntology(models.Model):
    go_index = models.CharField(max_length=100)
    go_id = models.CharField(max_length=100)
    go_name = models.CharField(max_length=200)
    go_definition = models.CharField(max_length=2000)
    go_synonym = []
    go_is_a = []
    go_part_of = []
    go_hierarcy = []
    def __unicode__(self):
        return self.title
