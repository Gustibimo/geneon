from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'searchgo_solr/$', views.searchgo_solr, name='searchgo_solr'),
    url(r'resultgo_solr/$', views.resultgo_solr, name='resultgo_solr'),
    url(r'detailgo/$', views.detailgo, name='detailgo'),
    url(r'popupresultgosolr/$', views.popup_resultgo_solr, name='popup_resultgo_solr'),
    url(r'viewitem/$', views.viewitem, name='viewitem'),
    url(r'menu/$', views.menu, name='menu'),
    url(r'viewitemblank/$', views.viewitemblank, name='viewitemblank'),
    url(r'popupdetail/$', views.popup_detail, name='popup_detail'),
]
