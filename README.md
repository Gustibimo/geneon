# **Django Ontology SOLR**

A web-based tool for gene ontology analysis.

**Instalasi**

Prerequisition:

- Python 3.4

- Django 1.9.0

- SOLR 5

- MySql

Library:

- MySqlDB or PyMySql

- MakoTemplate

- request

- pandas
