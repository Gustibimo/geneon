var App = function() {
    this.data = [];
    this.DOM = this.getDOM();
    this.initializeInput();
    this.initializeOutput();
    this.printData();
};
App.prototype.getDOM = function() {
    return {
        input: $('#image_file'),
        preview: $('#preview'),
        output: $('#output'),
        outputOptionMinify: $('#output_minify')
    };
};
App.prototype.getOptions = function() {
    return {
        align: {
            x: 'center',
            y: 'center'
        },
        offset: {
            left: 0,
            top: 24
        },
        edit: true
    };
};
App.prototype.initializeInput = function() {
    var _this = this;
    var $el = this.DOM.input;
    $el.on('change', function(e) {
        var el = $el.get(0);
        if (el.files.length > 0) {
            _this.loadPreview(el.files[0]);
        }
    });
};
App.prototype.initializeOutput = function() {
    var _this = this;
    var $el = this.DOM.outputOptionMinify;
    $el.on('change', function() {
        _this.printData();
    });
};
App.prototype.loadPreview = function(file) {
    var _this = this;
    var $el = this.DOM.preview;
    var reader = new FileReader();
    var i = 0;
    reader.onloadend = function() {
        while ($el.children().length) {
            $el.children().remove();
        }
        var img = new Image();
        img.src = reader.result;
        $el.append(img);
        var taggd = $el.children().taggd(_this.getOptions(), []);
        taggd.on('change', function() {
            _this.data = taggd.data;
            _this.printData();
        });
    };
    reader.readAsDataURL(file);
};
App.prototype.printData = function() {
    var $el = this.DOM.outputOptionMinify;
    var json = null;
    json = JSON.stringify(this.data);
    json = JSON.stringify(this.data, null, '\t');
    $el.html('var data = ' + json);
};	

new App();

function selectDefaultCombo(combo_id, default_value) {
    var combo_obj = document.getElementById(combo_id);
    if(combo_obj != undefined){
          for (var i = 0; i < combo_obj.length; i++) {
        if (combo_obj.options[i].value == default_value) {
            combo_obj.selectedIndex = i;
            return;
        }
    }
    }
}