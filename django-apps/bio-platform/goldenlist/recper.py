import pandas as pd
import requests
import operator
from operator import itemgetter

mRecallResult = {}
def run_eval(csv_file):
    dataframe = pd.read_csv(csv_file)
    
    for query in dataframe:
        try:
            query = query.lstrip()
            query_result = get_result(query)
            onto_retireved = get_onto_id(query_result)

            onto_goldenlist = []
            golden_list = pd.read_csv(query + ".csv")
            for item in golden_list:
                onto_goldenlist.append(item.strip())
                
            print(query)
            mRecallResult[query] = evaluation(onto_retireved, onto_goldenlist)
            print("")
        except Exception as e:
            print(e)
            continue


def get_result(query):
    solr_url = "http://localhost:8983/solr/geneontology/select"
    url = solr_url + "?q=" + query + "&start=0&rows=100000&wt=json"
    query_result = requests.get(url)
    return query_result

def get_onto_id(response):
    go_onto = []
    result = response.json()

    if result == None:
        return go_onto

    result_response = None
    if "response" in result:
        result_response = result["response"]

    if result_response == None:
        return go_onto

    if len(result_response["docs"]) > 0:
        num = 1
        for doc in result_response["docs"]:
            go_id = doc["id"]
            go_onto.append(go_id)

    return go_onto

def average_recall():
        avg_recall = {}
        sum_recall = {}
        titik_recall = 0
        while titik_recall <= 1:
            try:
                titik_recall = round(titik_recall,1)

                for item in mRecallResult:
                    for itemResult in mRecallResult[item]:
                        if itemResult == titik_recall:
                            if itemResult in sum_recall.keys():
                                print(mRecallResult[item][itemResult])
                                sum_recall[itemResult] = sum_recall[itemResult] + (mRecallResult[item][itemResult] * 100)
                            else:
                                print(mRecallResult[item][itemResult])
                                sum_recall[itemResult] = mRecallResult[item][itemResult] * 100

                titik_recall += 0.1
            except Exception as e:
                print(e)
                break

        for item in sum_recall:
            print(sum_recall[item]/20)
            
        return avg_recall

def evaluation(retrieved, golden_list):
    relevant_retrieved = 0

    for i in retrieved:
        if i in golden_list:
            relevant_retrieved += 1
        else:
            relevant_retrieved += 0

    retrieved2 = retrieved #retrieved[0:len(golden_list)]
    r_prec = 0
    for i in retrieved2:
        if i in golden_list:
            r_prec += 1
        else:
            r_prec += 0
    
    print('jumlah yang relevan dikembalikan sesuai banyak golden list = ', r_prec)
    r_precision = 1.0 * r_prec/len(retrieved2)
    print('Jumlah relevan yang dikembalikan = ', relevant_retrieved)
    #print('R-precision = ', r_precision)
    print('Jumlah dokumen relevan = ', len(golden_list))
    print('Jumlah dokumen yang dikembalikan = ', len (retrieved))

    precision = 1.0 * relevant_retrieved/len(retrieved)
    recall = 1.0 * relevant_retrieved/len(golden_list)

    print('precision biasa = ', precision)
    print('recall biasa = ', recall)

    prec2 = []
    rec2 = []
    relInt = 0
    for i in retrieved:
        if i in golden_list:
            relInt += 1
            precInt_beforeRound = 1.0 * relInt/(retrieved.index(i)+1)
            precInt = round(precInt_beforeRound,4)
            recInt_beforeRound = 1.0 * relInt/len(golden_list)
            recInt = round(recInt_beforeRound,4)
            
            prec2.extend([precInt])
            rec2.extend([recInt])
        else:
            precInt_beforeRound = 1.0 * relInt / (retrieved.index(i) + 1)
            precInt = round(precInt_beforeRound, 4)
            recInt_beforeRound = 1.0 * relInt / len(golden_list)
            recInt = round(recInt_beforeRound, 4)
            
            prec2.extend([precInt])
            rec2.extend([recInt])

    print('ini presisi ', prec2)
    print('ini recall ', rec2)

    zips = zip(prec2,rec2)
    a = list(zips)
    titik_recall = 0
    recall_point = {}
    
    while titik_recall <= 1:
        try:
            d = 0
            c = []
            titik_recall = round(titik_recall,1)
            while d < len(a):
                if a[d][1] > titik_recall:
                    c.extend(a[d])
                d += 1
            
            x = 0
            fix = []
            while x < len(c):
                if x%2 == 0:
                    fix.append(c[x])
                x += 1

            maks = 0
            if len(fix):
                maks = max(fix)
                
            print(titik_recall, ' : ', maks)
            recall_point[titik_recall] = maks
            titik_recall += 0.1
        except Exception as e:
            print(e)
            break
        
    return recall_point

run_eval("kueri.csv")
#average_recall()
