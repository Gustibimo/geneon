# apps/public/media/views.py
import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf
from django.conf import settings

from apps.private.functions.services import FunctionService as functionService
from apps.private.image_tagging.services import ImageTagingService as imageTagingService

# variable
base_url = settings.BASE_URL
master_view = settings.PUBLIC_MASTER_VIEW
template_path = 'public/media/'

# Create your views here.
def view(request, pk):
    csrf_token = csrf.get_token(request)
    image = imageTagingService.GetImageTaging(pk)
    menu = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token,'model' : image, 'base_url':base_url})
    content = render_to_string("public/search/search.html", {'csrf_token':csrf_token,'base_url':base_url, 'content' : menu})
    return render(request, master_view, {'content':content,'base_url':base_url})
