# apps/public/media/views.py
import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf
from django.conf import settings
from django.http import JsonResponse
from apps.private.functions.services import FunctionService as functionService
from apps.private.image_tagging.services import ImageTagingService as imageTagingService
from lib.gomodel import GOntology
from lib.biosolr import BioSolr

# variable
base_url = settings.BASE_URL
master_view = settings.PUBLIC_MASTER_VIEW
popup_master_view = settings.POPUP_MASTER_VIEW
solr_url = settings.SOLR_SELECT_URL
template_path = 'public/search/'
str_parent = []
span_parent = {}

# Create your views here.
def result(request):
    csrf_token = csrf.get_token(request)
    expand_query = request.POST.get("expand_query", "")
    query = request.POST.get("query", "")

    bio_solr = BioSolr()
    bio_solr.solr_url = solr_url

    result = None
    if expand_query:
        result = bio_solr.getListResultExpandedQuery(query, 10)
        query = bio_solr.last_query
    else:
        result = bio_solr.getListResult(query)

    data = {'csrf_token':csrf_token,
            'base_url':base_url,
            'model' : result,
            'detail_url' : 'detail',
            'query' : query}

    search_content = render_to_string("public/search/result.html", data)
    content = render_to_string("public/search/search.html", {'csrf_token':csrf_token,'base_url':base_url, 'content' : search_content, 'query' : query, 'expand_query' : expand_query})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request):
    query = request.GET.get("query", "")
    id = request.GET.get("id", "")
    go_id = id
    query_result = None

    bio_solr = BioSolr()
    bio_solr.solr_url = solr_url
    result = bio_solr.getObjectResult(id)
    csrf_token = csrf.get_token(request)

    str_parent = bio_solr.str_parent
    span_parent = bio_solr.span_parent

    data = {'csrf_token':csrf_token,
            'base_url':base_url,
            'model' : result,
            'query_result':query_result,
            'dict_hierarcys':str_parent,
            'span_parent':span_parent,
            'tab' : (len(str_parent)+1)*10}

    search_content = render_to_string("public/search/detail.html", data)
    content = render_to_string("public/search/search.html", {'csrf_token':csrf_token,'base_url':base_url, 'content' : search_content, 'query' : query})
    str_parent.clear()
    span_parent.clear()
    return render(request, master_view, {'content':content,'base_url':base_url})

def popupresult(request):
    csrf_token = csrf.get_token(request)

    query = request.GET.get("query", "")

    bio_solr = BioSolr()
    bio_solr.solr_url = solr_url
    result = bio_solr.getListResult(query)

    content = render_to_string("public/search/result.html", {'csrf_token':csrf_token,'base_url':base_url, 'model' : result, 'detail_url' : 'popupdetail'})

    return render(request, popup_master_view, {'content':content,'base_url':base_url})

def popupdetail(request):
    query = request.GET.get("query", "")
    id = request.GET.get("id", "")
    query_result = None

    bio_solr = BioSolr()
    bio_solr.solr_url = solr_url
    result = bio_solr.getObjectResult(id)

    csrf_token = csrf.get_token(request)

    str_parent = bio_solr.str_parent
    span_parent = bio_solr.span_parent

    data = {'csrf_token':csrf_token,
            'base_url':base_url,
            'model' : result,
            'query_result':query_result,
            'dict_hierarcys':str_parent,
            'span_parent':span_parent,
            'tab' : (len(str_parent)+1)*10}

    content = render_to_string("public/search/detail.html", data)
    str_parent.clear()
    span_parent.clear()
    return render(request, popup_master_view, {'content':content,'base_url':base_url})

def onto_child(request):
    go_id = request.GET.get("id")
    if go_id == "" or go_id == None:
        return JsonResponse({"error":"1"})

    bio_solr = BioSolr()
    bio_solr.solr_url = solr_url

    go_objects = bio_solr.getOntoChilds(go_id[2:])

    json_object = []
    for item in go_objects:
        obj = {item.go_id:item.go_name}
        json_object.append(obj)

    return JsonResponse(json_object, safe=False)
