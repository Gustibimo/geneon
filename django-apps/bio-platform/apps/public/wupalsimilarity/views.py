# apps/public/media/views.py
import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf
from django.conf import settings

from apps.private.functions.services import FunctionService as functionService
from apps.private.image_tagging.services import ImageTagingService as imageTagingService
from lib.gomodel import GOntology
from lib.biosolr import BioSolr

# variable
base_url = settings.BASE_URL
master_view = settings.PUBLIC_MASTER_VIEW
popup_master_view = settings.POPUP_MASTER_VIEW
solr_url = settings.SOLR_SELECT_URL
template_path = 'public/wupalsimilarity/'
str_parent = []
span_parent = {}

# Create your views here.

def index(request):
    csrf_token = csrf.get_token(request)

    query_1 = request.POST.get("query_1","")
    query_2 = request.POST.get("query_2","")

    query_result = None
    if query_1 != None and query_1 != "":
        url = solr_url + "?q=id:\"" + query_1 + "\"&wt=json"
        query_result = requests.get(url)

    result_1 = None
    if query_result != None:
        result_1 = bind_json_to_object(query_result, query_1)

    result_1_parents = []
    if result_1 != None:
        result_1_parents.append(result_1)
        result_1_parents = get_object_parents(result_1, result_1_parents)

    query_result = None
    if query_2 != None and query_2 != "":
        url = solr_url + "?q=id:\"" + query_2 + "\"&wt=json"
        query_result = requests.get(url)

    result_2 = None
    if query_result != None:
        result_2 = bind_json_to_object(query_result, query_2)

    result_2_parents = []
    if result_2 != None:
        result_2_parents.append(result_2)
        result_2_parents = get_object_parents(result_2, result_2_parents)

    index_wp = get_wupalmer_n(result_1_parents, result_2_parents)

    wp_sim_1 = 0
    wp_sim_2 = 0
    n_1 = len(result_1_parents) - 1
    n_2 = len(result_2_parents) - 1
    if (n_1 + n_2) > 0:
        wp_sim_1 = 2 * index_wp / (n_1 + n_2)

    if (n_1 + n_2 + (2 * index_wp)) > 0:
        wp_sim_2 = (2 * index_wp) / (n_1 + n_2 + (2 * index_wp))

    data = {
        'csrf_token':csrf_token,
        'base_url':base_url,
        'query_1' : query_1,
        'query_2' : query_2,
        'result_1' : result_1,
        'result_2' : result_2,
        'result_1_parents' : result_1_parents,
        'result_2_parents' : result_2_parents,
        'index_wp' : index_wp,
        'n_1' : n_1,
        'n_2' : n_2,
        'wp_sim_1' : wp_sim_1,
        'wp_sim_2' : wp_sim_2,
    }

    content = render_to_string("public/wupalsimilarity/index.html", data)
    return render(request, master_view, {'content':content,'base_url':base_url})

def get_wupalmer_n(hierarcy_1, hierarcy_2):

    index_wp = 0
    hier_id_1 = []
    for item in hierarcy_1:
        hier_id_1.append(item.go_id)

    index = -1
    for item in hierarcy_2:
        try:
            index = hier_id_1.index(item.go_id)
        except:
            index = -1

        if index > -1:
            if (index + 1) != len(hierarcy_2):
                index_wp = index_wp + 1

    return index_wp

def get_object_parents(obj, parents):
    if obj == None:
        return parents

    if obj.go_parent != None:
        parents.append(obj.go_parent)
        return get_object_parents(obj.go_parent, parents)
    else:
        return parents

def bind_json_to_object(response, id):
    go_onto = None
    result = response.json()
    if result == None:
        return go_onto

    result_response = None
    if "response" in result:
        result_response = result["response"]

    if result_response == None:
        return go_onto

    if len(result_response["docs"]) > 0:
        doc = result_response["docs"][0]
        gene_ontology = GOntology()
        if "id" in doc:
            gene_ontology.go_id = doc["id"]
        if "go_name" in doc:
            gene_ontology.go_name = doc["go_name"]
        if "go_definition" in doc:
            gene_ontology.go_definition = doc["go_definition"]

        if "is_a" in doc:
            gene_ontology.go_is_a = []
            num = 1
            for is_a in doc["is_a"]:
                if num > 1:
                    break
                gene_ontology.go_is_a.append(is_a[31:])

                object_of = get_solr_hierarchy(is_a[31:])
                gene_ontology.go_parent = None
                if object_of != None:
                    str_parent.append(object_of)
                    gene_ontology.go_parent = object_of
                    if len(str_parent) > 0:
                        str_span = 0

                        for item in str_parent:
                            str_span += 10

                        span_parent[object_of.go_id] = str_span
                    num += 1

        if "go_synonym" in doc:
            gene_ontology.go_synonym = doc["go_synonym"]

        go_onto = gene_ontology

    return go_onto

def get_solr_hierarchy(id):
    go_onto = None

    if id == "all":
        return None

    if id != None and id != "":
        url = solr_url + "?q=id:\"" + id + "\"&wt=json"
        query_result = requests.get(url)

        if query_result != None:
            go_onto = bind_json_to_object(query_result, id)

    return go_onto
