from django.conf.urls import url

from .home import views as home
from .media import views as media
from .search import views as search
from .wupalsimilarity import views as wupalsimilarity

urlpatterns = [
    url(r'^$', home.index , name='home_index'),
    url(r'media/view/(?P<pk>\d+)$', media.view , name='media_view'),
    url(r'search/result', search.result , name='search_result'),
    url(r'wupalsimilarity', wupalsimilarity.index , name='wupalsimilarity'),
    url(r'search/detail', search.detail , name='search_detail'),
    url(r'search/popupresult/$', search.popupresult , name='search_popup_result'),
    url(r'search/popupdetail/$', search.popupdetail , name='search_popup_detail'),
    url(r'search/getontochild/$', search.onto_child , name='search_getontochild'),
]
