from datetime import datetime
from django.db import models
from django.utils import timezone
from apps.private.user_groups.models import UserGroups
# Create your models here.
class Users(models.Model):
    user_name = models.CharField(max_length=100, default='', editable=True, primary_key=True)
    password = models.CharField(max_length=100, default='', editable=True)
    name =  models.CharField(max_length=100, default='', editable=True)
    email = models.EmailField(max_length=100, default='', editable=True)
    user_group_id = models.OneToOneField(UserGroups)
    is_active = models.BooleanField(max_length=1, default=False, editable=True)
    created_date = models.DateTimeField(default=timezone.now(), blank=True)
    updated_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.name
