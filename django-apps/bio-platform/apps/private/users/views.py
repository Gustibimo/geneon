# apps/private/users/views.py
import os
import os.path
import requests
from datetime import datetime

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from apps.private.users.models import Users
from apps.private.user_groups.models import UserGroups
from apps.private.users.services import UserService as user_service
from apps.private.user_groups.services import UserGroupService as user_group_service
from lib.basecontroller import BaseController as base_controller
# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/users/'
function_id = 'users'

# Create your views here.
def index(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    objs = user_service.GetUsers(user_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowCreate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = Users()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            user_service.SaveUser(obj)
            return HttpResponseRedirect('/manage/users/detail/' + str(obj.pk))

    usergroup_combobox = get_usergroups(request, None)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url, 'usergroup_combobox' : usergroup_combobox})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowUpdate(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = Users()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            user_service.SaveUser(obj)
            return HttpResponseRedirect('/manage/users/detail/' + str(obj.pk))
    else:
        obj = user_service.GetUser(pk)

    usergroup_combobox = get_usergroups(request, obj.user_group_id)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url, 'usergroup_combobox' : usergroup_combobox})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)

    obj = user_service.GetUser(pk)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def delete(request, pk):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    if base_controller.isAllowDelete(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    user_service.DeleteUser(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/user")

def bind_data_to_object(request):
    obj = Users()
    id = request.POST.get("username", "")
    obj.password = request.POST.get("password", "")
    if id != None and id != 'None':
        obj.pk = id
        if obj.password == None or obj.password == '':
            obj_old = user_service.GetUser(id)
            obj.password = obj_old.password
            obj.created_date = obj_old.created_date

    obj.name = request.POST.get("name", "")
    obj.email = request.POST.get("email", "")

    is_active = request.POST.get("is_active", "")
    if is_active != None:
        obj.is_active = bool(is_active)

    user_group_id = request.POST.get("user_group_id", "")
    if user_group_id != "":
        obj_usergroup = UserGroups()
        obj_usergroup.pk = user_group_id
        obj.user_group_id = obj_usergroup

    return obj

def validate_data(request):
    return True

def get_usergroups(request, selected_id):
    objs = user_group_service.GetUserGroups(user_group_service)
    combobox_usergroup = ""

    combobox_usergroup += "<select name='user_group_id' id='user_group_id' class='form-control input-sm'>"
    if objs != None:
        for row in objs:
            combobox_usergroup += "<option value='" + str(row.pk) + "'>" + row.name + "</option>"
    combobox_usergroup += "</select>"

    return combobox_usergroup
