from .models import Modules
from .daos import ModulesDao as moduleDao
class ModuleService():

    def SaveModule(obj):
        result = moduleDao.SaveModule(obj)
        return result

    def GetModule(id):
        result = moduleDao.GetModule(id)
        return result

    def GetModules(self):
        result = moduleDao.GetModules(self)
        return result

    def DeleteModule(id):
        result = moduleDao.DeleteModule(id)
        return result
