from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import Modules

class ModulesDao():
    def SaveModule(obj):
        try:
            obj.save()
            return True
        except IntegrityError as e:
            return e.message

    def DeleteModule(id):
        try:
            obj = Modules.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetModule(id):
        try:
            obj = Modules.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetModules(self):
        try:
            obj = Modules.objects.all()
            return obj
        except IntegrityError as e:
            return e.message
