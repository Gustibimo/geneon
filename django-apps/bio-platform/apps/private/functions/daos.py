from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError

from .models import Functions

class FunctionsDao():
    def SaveFunction(obj):
        try:
            obj.save()
            return True
        except IntegrityError as e:
            return e.message

    def DeleteFunction(id):
        try:
            obj = Functions.objects.get(pk=id)
            return obj.delete()
        except IntegrityError as e:
            return e.message

    def GetFunction(id):
        try:
            obj = Functions.objects.get(pk=id)
            return obj
        except IntegrityError as e:
            return e.message

    def GetFunctions(self):
        try:
            obj = Functions.objects.all()
            return obj
        except IntegrityError as e:
            return e.message
