from django.db import models

# Create your models here.
class UserGroups(models.Model):
    name = models.CharField(max_length=100, default='', editable=True)

    def __unicode__(self):
        return self.name
