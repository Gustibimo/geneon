from .models import UserGroups
from .daos import UserGroupsDao as moduleDao
class UserGroupService():

    def SaveUserGroup(obj, obj_detail):
        result = moduleDao.SaveUserGroup(obj, obj_detail)
        return result

    def UpdateUserGroup(obj, obj_detail):
        result = moduleDao.UpdateUserGroup(obj, obj_detail)
        return result

    def GetUserGroup(id):
        result = moduleDao.GetUserGroup(id)
        return result

    def GetUserGroups(self):
        result = moduleDao.GetUserGroups(self)
        return result

    def DeleteUserGroup(id):
        result = moduleDao.DeleteUserGroup(id)
        return result
