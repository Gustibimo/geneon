# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2017-02-25 11:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=100)),
                ('created_by', models.CharField(default='', max_length=100)),
                ('created_date', models.DateTimeField()),
                ('updated_by', models.CharField(default='', max_length=100)),
                ('updated_date', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
