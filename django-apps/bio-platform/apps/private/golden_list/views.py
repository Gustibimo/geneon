# apps/private/golden_list/views.py
import os
import os.path
import requests
import html

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse, HttpResponseRedirect
from django.middleware import csrf
from django.conf import settings

from .models import GoldenList
from .services import GoldenListService as golden_list_service

# variable
base_url = settings.BASE_URL
master_view = settings.PRIVATE_MASTER_VIEW
template_path = 'private/golden_list/'

# Create your views here.
def index(request):
    csrf_token = csrf.get_token(request)
    objs = golden_list_service.GetGoldenLists(golden_list_service)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token, 'model' : objs})
    return render(request, master_view, {'content':content,'base_url':base_url})

def create(request):
    csrf_token = csrf.get_token(request)

    obj = GoldenList()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            golden_list_service.SaveGoldenList(obj)
            return HttpResponseRedirect('/manage/imagetaging/detail/' + str(obj.pk))

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj})
    return render(request, master_view, {'content':content,'base_url':base_url})

def edit(request, pk):
    csrf_token = csrf.get_token(request)

    obj = GoldenList()
    if request.method == 'POST':
        if validate_data(request):
            obj = bind_data_to_object(request)
            golden_list_service.SaveGoldenList(obj)
            return HttpResponseRedirect('/manage/imagetaging/detail/' + str(obj.pk))
    else:
        obj = golden_list_service.GetGoldenList(pk)

    content = render_to_string(template_path + "input.html", {'csrf_token':csrf_token, 'model' : obj,'base_url':base_url})
    return render(request, master_view, {'content':content,'base_url':base_url})

def detail(request, pk):
    csrf_token = csrf.get_token(request)

    obj = golden_list_service.GetGoldenList(pk)
    taging_data = None
    if obj != None:
        taging_data = html.unescape(obj.taging_data)

    content = render_to_string(template_path + "detail.html", {'csrf_token':csrf_token, 'model' : obj, 'taging_data' : taging_data, 'base_url':base_url})
    return render(request, master_view, {'content':content, 'show_taggd' : True ,'base_url':base_url})

def delete(request, pk):
    csrf_token = csrf.get_token(request)
    golden_list_service.DeleteGoldenList(pk)
    content = render_to_string(template_path + "index.html", {'csrf_token':csrf_token})
    return redirect(base_url + "manage/imagetaging")

def bind_data_to_object(request):
    obj = GoldenList()
    id = request.POST.get("id", "")
    if id != None and id != 'None':
        obj.pk = id
    preview = request.POST.get("preview", "")
    obj.name = request.POST.get("name", "")

    try:
        obj.image_file = request.FILES['image_file']
    except:
        obj.image_file = preview

    obj.taging_data = request.POST.get("output_minify", "")

    return obj

def validate_data(request):
    return True
