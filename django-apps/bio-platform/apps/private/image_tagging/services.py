from .models import ImageTaging
from .daos import ImageTagingDao as imageTagingDao
class ImageTagingService():

    def SaveImageTaging(obj):
        result = imageTagingDao.SaveImageTaging(obj)
        return result

    def GetImageTaging(id):
        result = imageTagingDao.GetImageTaging(id)
        return result

    def GetImageTagings(self):
        result = imageTagingDao.GetImageTagings(self)
        return result

    def DeleteImageTaging(id):
        result = imageTagingDao.DeleteImageTaging(id)
        return result
