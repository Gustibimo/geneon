from django.apps import AppConfig


class ImageTaggingConfig(AppConfig):
    name = 'image_tagging'
