from django.db import models
from apps.private.functions.models import Functions

# Create your models here.
class ImageTaging(models.Model):
    name = models.CharField(max_length=100, default='', editable=True)
    image_file = models.FileField(upload_to='static/images/%Y/%m/%d')
    taging_data = models.CharField(max_length=1000, default='', editable=True)

    def __unicode__(self):
        return self.name
