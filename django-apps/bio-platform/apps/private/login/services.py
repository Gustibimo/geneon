from apps.private.users.models import Users
from .daos import LoginDao as loginDao

class LoginService():

    def GetUser(self, id):
        result = loginDao.GetUser(id)
        return result

    def GetPrivilege(self, functionid, usergroupid):
        result = loginDao.GetUser(functionid, usergroupid)
        return result
