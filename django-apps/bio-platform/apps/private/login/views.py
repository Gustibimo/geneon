# apps/private/login/views.py
import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.shortcuts import redirect
from django.middleware import csrf
from django.conf import settings

from apps.private.users.models import Users
from lib.basecontroller import BaseController
from .services import LoginService as login_service

from apps.private.privileges.models import Privileges
from apps.private.privileges.services import PrivilegeService as privilege_service
from apps.private.privileges.filter import PrivilegeFilter
from apps.private.user_groups.services import UserGroupService as user_group_service

# Create your views here.
base_url = settings.BASE_URL
template_path = 'private/login/'

def index(request):
    base_controller = BaseController()
    if base_controller.isLogin(request) == True:
        return redirect(base_url + "manage/")

    csrf_token = csrf.get_token(request)
    content = ""

    username = ""
    password = ""
    login_success = False
    msg_error = ""

    if request.method == 'POST':
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")

        if username != "":
            obj = login_service.GetUser(login_service, username)
            if obj != None:
                if password == obj.password and obj.is_active == True:
                    login_success = True
                    request.session["is_login"] = True
                    request.session["user_name"] = obj.user_name
                    obj_detail =  privilege_service.GetPrivileges(privilege_service, None)

                    obj_filter = {}
                    obj_filter["user_group_id"] = obj.user_group_id.pk
                    obj_filtered = PrivilegeFilter(obj_filter,queryset=obj_detail)

                    content = ""
                    privileges = {}
                    if obj_filtered != None:
                        for item in obj_filtered.qs:
                            if item != None:
                                if item.is_show == True:
                                    if item.function_id != None:
                                        key = item.function_id.function_id
                                        if key not in privileges:
                                            privileges[item.function_id.function_id + '_is_allow_read'] = item.is_allow_read
                                            privileges[item.function_id.function_id + '_is_allow_create'] = item.is_allow_create
                                            privileges[item.function_id.function_id + '_is_allow_update'] = item.is_allow_update
                                            privileges[item.function_id.function_id + '_is_allow_delete'] = item.is_allow_delete

                    request.session["privileges"] = privileges
            if login_success:
                return redirect(base_url + "manage/")
            else:
                msg_error = "Username or Password incorect"
                if obj != None:
                    if obj.is_active == False:
                        msg_error = "Your User is inactive, please contact your administrator"
        else:
            msg_error = "Please Insert Username or Password"

    return render(request, template_path + "index.html", {'content':content,'base_url':base_url, 'msg_error' : msg_error})

def logout(request):
    try:
        del request.session['is_login']
        del request.session['user_name']
        del request.session["privileges"]
    except KeyError:
        pass

    return redirect(base_url + "manage/login")
