# apps/private/home/views.py
import os
import os.path
import requests

from django.shortcuts import render, redirect
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf

from lib.basecontroller import BaseController as base_controller

from apps.private.privileges.models import Privileges
from apps.private.privileges.services import PrivilegeService as privilege_service
from apps.private.privileges.filter import PrivilegeFilter
from apps.private.user_groups.services import UserGroupService as user_group_service

# Create your views here.
base_url = 'http://localhost:8000/'
function_id = 'home'

def index(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")
    if base_controller.isAllowRead(base_controller, request, function_id) == False:
        return redirect(base_url + "manage/notallowed/")

    csrf_token = csrf.get_token(request)
    content = ""
    is_login = request.session['is_login']
    return render(request, "shared/master.html", {'content':content,'base_url':base_url,'is_login' : is_login})

def get_menu(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    user = base_controller.getUser(base_controller, request)
    id = ""
    if user != None:
        if user.user_group_id != None:
            id = user.user_group_id.pk

    obj_detail =  privilege_service.GetPrivileges(privilege_service, None)

    obj_filter = {}
    obj_filter["user_group_id"] = id
    obj_filtered = PrivilegeFilter(obj_filter,queryset=obj_detail)

    content = ""
    modules = {}
    function_id = request.session["functionid"]
    active_key = ""
    if obj_filtered != None:
        for item in obj_filtered.qs:
            if item != None:
                if item.is_show == True:
                    if item.module_id != None:
                        key = item.module_id.name
                        if item.function_id != None:
                            if item.function_id.function_id == function_id:
                                active_key = key
                        if key in modules:
                            modules[item.module_id.name].append(item.function_id)
                        else:
                            modules[item.module_id.name] = []
                            modules[item.module_id.name].append(item.function_id)


    return  render(request, "shared/leftmenu.html",{'content':modules,'base_url':base_url,'function_id':function_id,'active_key':active_key})

def getloginname(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    user = base_controller.getUser(base_controller, request)
    username = ""
    if user != None:
        username = user.name

    if username == "":
        username = 'User Login'

    return render(request, "shared/blank.html", {'content':username,'base_url':base_url})

def notallowed(request):
    if base_controller.isLogin(base_controller, request) == False:
        return redirect(base_url + "manage/login/")

    privileges = request.session["privileges"]
    alert_msg ='<div class="alert alert-warning">'
    alert_msg +='<h4><i class="icon fa fa-warning"></i> Alert!</h4>'
    alert_msg +='<h3>Forbidden</h3>'
    alert_msg +='</div>'
    content = render_to_string("shared/blank.html", {'content':alert_msg,'base_url':base_url})
    return render(request, "shared/master.html", {'content':content,'base_url':base_url})
