import pandas as pd
import requests
import operator
from operator import itemgetter

class RecallPercision:

    solrUrl = ""
    mRecallResult = {}
    mSumRecall = {}
    mRetrieved = {}
    mGoldenList = {}
    mQueryRelevant = {}
    mRecallQuery = {}
    mPrecisionQuery = {}

    def run_eval(self, path, csv_file):
        dataframe = pd.read_csv(path + csv_file)

        for query in dataframe:
            try:
                query = query.lstrip()
                query_result = self.get_result(query)
                onto_retireved = self.get_onto_id(query_result)

                onto_goldenlist = []
                golden_list = pd.read_csv(path + query + ".csv")
                for item in golden_list:
                    onto_goldenlist.append(item.strip())

                self.mRecallResult[query] = self.interpolated_max(onto_retireved, onto_goldenlist)
                self.mRetrieved[query] = onto_retireved
                self.mGoldenList[query] = onto_goldenlist
                self.mRecallQuery[query] = self.get_recall(onto_retireved, onto_goldenlist)
                self.mPrecisionQuery[query] = self.get_precision(onto_retireved, onto_goldenlist)
                self.mQueryRelevant[query] = self.get_relevant_document(onto_retireved, onto_goldenlist)
            except Exception as e:
                continue

        return self.mRecallResult

    def get_result(self, query):
        solr_url = self.solrUrl
        url = solr_url + "?q=" + query + "&start=0&rows=100000&wt=json"
        query_result = requests.get(url)
        return query_result

    def get_onto_id(self, response):
        go_onto = []
        result = response.json()

        if result == None:
            return go_onto

        result_response = None
        if "response" in result:
            result_response = result["response"]

        if result_response == None:
            return go_onto

        if len(result_response["docs"]) > 0:
            num = 1
            for doc in result_response["docs"]:
                go_id = doc["id"]
                go_onto.append(go_id)

        return go_onto

    def average_recall(self):
        sum_recall = {}
        titik_recall = 0
        avg_recall = {}
        while titik_recall <= 1:
            try:
                titik_recall = round(titik_recall,1)

                for item in self.mRecallResult:
                    for itemResult in self.mRecallResult[item]:
                        if itemResult == titik_recall:
                            if itemResult in sum_recall.keys():
                                sum_recall[itemResult] = sum_recall[itemResult] + (self.mRecallResult[item][itemResult] * 100)
                            else:
                                sum_recall[itemResult] = (self.mRecallResult[item][itemResult] * 100)

                titik_recall += 0.1
                titik_recall = round(titik_recall,1)

            except Exception as e:
                print(e)
                break

        r_standar = 0
        while r_standar <= 1.0:
            golden_list_count = len(self.mGoldenList)
            avg_recall[r_standar] = (sum_recall[r_standar]/golden_list_count)
            self.mSumRecall[r_standar] = sum_recall[r_standar]
            r_standar += 0.1
            r_standar = round(r_standar,1)

        return avg_recall

    def evaluation(self, retrieved, golden_list):
        recall_point = {}
        relevant_retrieved = 0

        for i in retrieved:
            if i in golden_list:
                relevant_retrieved += 1
            else:
                relevant_retrieved += 0

        retrieved2 = retrieved[0:len(golden_list)]

        r_prec = 0
        for i in retrieved2:
            if i in golden_list:
                r_prec += 1
            else:
                r_prec += 0

        r_precision = 1.0 * r_prec/len(retrieved2)
        precision = 1.0 * relevant_retrieved/len(retrieved)
        recall = 1.0 * relevant_retrieved/len(golden_list)

        prec2 = []
        rec2 = []
        relInt = 0
        for i in retrieved:
            if i in golden_list:
                relInt += 1
                precInt_beforeRound = 1.0 * relInt/(retrieved.index(i)+1)
                precInt = round(precInt_beforeRound,4)
                recInt_beforeRound = 1.0 * relInt/len(golden_list)
                recInt = round(recInt_beforeRound,4)
                prec2.extend([precInt])
                rec2.extend([recInt])
            else:
                precInt_beforeRound = 1.0 * relInt / (retrieved.index(i) + 1)
                precInt = round(precInt_beforeRound, 4)
                recInt_beforeRound = 1.0 * relInt / len(golden_list)
                recInt = round(recInt_beforeRound, 4)

                prec2.extend([precInt])
                rec2.extend([recInt])

        zips = zip(prec2,rec2)
        a = list(zips)
        titik_recall = 0

        while titik_recall <= 1:
            try:
                d = 0
                c = []
                titik_recall = round(titik_recall,1)
                while d < len(a):
                    if a[d][1] > titik_recall:
                        c.extend(a[d])
                    d += 1

                x = 0
                fix = []
                while x < len(c):
                    if x%2 == 0:
                        fix.append(c[x])
                    x += 1

                maks = 0
                if len(fix):
                    maks = max(fix)

                recall_point[titik_recall] = maks
                #print(titik_recall, ' : ', maks)
                titik_recall += 0.1
            except Exception as e:
                print(e)
                break

        return recall_point

    def get_relevant_document(self, retrieved_list, golden_list):
        retrieved = 0
        relevant = 0
        for i in retrieved_list:
            retrieved += 1
            if i in golden_list:
                relevant += 1

        return relevant

    def get_relevant_item_count(self, retrieved, golden_list):
        relevant_item_retrieved = 0
        for item in retrieved:
            if item in golden_list:
                relevant_item_retrieved = relevant_item_retrieved + 1

        return relevant_item_retrieved

    def get_precision(self, retrieved, golden_list):
        item_retrieved = len(retrieved)

        relevant_item_retrieved = self.get_relevant_item_count(retrieved, golden_list)

        precision_point = 0
        if item_retrieved > 0:
            precision_point = (relevant_item_retrieved / item_retrieved)

        return precision_point

    def get_recall(self, retrieved, golden_list):
        item_relevant = len(golden_list)

        relevant_item_retrieved = self.get_relevant_item_count(retrieved, golden_list)

        recall_point = 0
        if item_relevant > 0:
            recall_point = (relevant_item_retrieved / item_relevant)

        return recall_point

    def interpolated_max(self, retrieved_list, golden_list):
        retrieved = 0
        relevant = 0
        golden_list_count = len(golden_list)
        recall = []
        precision = []
        retrieved = 0

        for i in retrieved_list:
            retrieved += 1
            if i in golden_list:
                relevant += 1
                recall_point = (relevant / golden_list_count)
                recall.append(round(recall_point,4))
                precision_point = (relevant / retrieved)
                precision.append(round(precision_point,4))

        zips = zip(precision,recall)
        a = list(zips)
        titik_recall = 0
        recall_point = {}

        while titik_recall <= 1:
            try:
                d = 0
                c = []
                titik_recall = round(titik_recall,1)
                while d < len(a):
                    if a[d][1] >= titik_recall:
                        c.extend(a[d])
                    d += 1

                x = 0
                fix = []
                while x < len(c):
                    if x%2 == 0:
                        fix.append(c[x])
                    x += 1

                maks = 0
                if len(fix):
                    maks = max(fix)

                recall_point[titik_recall] = maks
                titik_recall += 0.1
            except Exception as e:
                print(e)
                break
        return recall_point
