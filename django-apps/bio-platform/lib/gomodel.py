from django.db import models

class GOntology(models.Model):
    go_index = models.CharField(max_length=100)
    go_id = models.CharField(max_length=100)
    go_id_alt = models.CharField(max_length=100)
    go_name = models.CharField(max_length=200)
    go_definition = models.CharField(max_length=2000)
    go_synonym = []
    go_is_a = []
    go_part_of = []
    go_hierarcy = []
    go_parent = models.ForeignKey("self", models.SET_NULL, blank=True, null=True)

    def __unicode__(self):
        return self.title
