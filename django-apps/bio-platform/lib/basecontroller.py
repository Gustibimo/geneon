import os
import os.path
import requests

from django.shortcuts import render
from django.template.loader import get_template, render_to_string
from django.template import Template, Context
from django.http import HttpResponse
from django.middleware import csrf

from apps.private.login.services import LoginService

class BaseController:

    def isLogin(self, request):
        is_login = False
        if request != None:
            if "is_login" in request.session:
                is_login = request.session["is_login"]

        return is_login

    def isAllowRead(self, request, functionid):
        is_allow = False
        if request != None:
            if "privileges" in request.session:
                privileges = request.session["privileges"]
                if privileges != None:
                    if functionid == 'home':
                        is_allow = True
                        request.session["functionid"] = functionid

                    if functionid + '_is_allow_read' in privileges:
                        is_allow = privileges[functionid + '_is_allow_read']
                        request.session["functionid"] = functionid

        return is_allow

    def isAllowCreate(self, request, functionid):
        is_allow = False
        if request != None:
            if "privileges" in request.session:
                privileges = request.session["privileges"]
                if privileges != None:
                    if functionid + '_is_allow_create' in privileges:
                        is_allow = privileges[functionid + '_is_allow_create']
                        request.session["functionid"] = functionid

        return is_allow

    def isAllowUpdate(self, request, functionid):
        is_allow = False
        if request != None:
            if "privileges" in request.session:
                privileges = request.session["privileges"]
                if privileges != None:
                    if functionid + '_is_allow_update' in privileges:
                        is_allow = privileges[functionid + '_is_allow_update']
                        request.session["functionid"] = functionid

        return is_allow

    def isAllowDelete(self, request, functionid):
        is_allow = False
        if request != None:
            if "privileges" in request.session:
                privileges = request.session["privileges"]
                if privileges != None:
                    if functionid + '_is_allow_delete' in privileges:
                        is_allow = privileges[functionid + '_is_allow_delete']
                        request.session["functionid"] = functionid

        return is_allow

    def getUser(self, request):

        user_name = ""
        if request != None:
            if "user_name" in request.session:
                user_name = request.session["user_name"]

        obj = None
        if user_name != "":
            login_service = LoginService()
            obj = login_service.GetUser(user_name)

        return obj
